#!/bin/bash

# Create user
ansible all -u root -e ansible_password=password -m user -a "name=ansible2 shell=/bin/bash"

# Add user to sudoers
ansible all -u root -e ansible_password=password -m copy -a "content='ansible2 ALL=(ALL) NOPASSWD: ALL' dest=/etc/sudoers.d/ansible2"

# Copy ssh pub kay
ansible all -u root -e ansible_password=password -m authorized_key -a "user=ansible2 key='{{ lookup('file', '/home/ansible2/.ssh/id_rsa.pub') }}'"

# Test
ansible all -m ping

# remove old repo files and deploy new one
ansible anclient03 -u root -e ansible_password=password --playbook-dir=/home/ansible2 -m include_role -a "name=initial_setup"

# install python3
